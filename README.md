# LAB_GEO_AGE01

Jupyter Notebook per calcolare l' eta' della Terra secondo Kelvin

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/npa-jnotebooks%2Flab_geo_age01/HEAD?labpath=UNIMIB_GEO_AGE01_kelvin_theory.ipynb)
