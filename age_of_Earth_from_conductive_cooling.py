import math
from math import sqrt, log10, exp, cos
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def npa_age_conductive_cooling_1(T_0=1000, k=4, grad=0.036):
    
    rho=5510      # Earth mean density (in kg/m3)
    c=1260       # Specific heat for a mixture of mantle components (in J/kg/K)
    
    sec_per_year=31536000
               
    eta_calc= (( sqrt( (rho*c)/(3.14157*k) ) * (T_0) * (1/grad) )**2 ) / sec_per_year  
        
    print('\n Eta\' della Terra in anni, per ottenere il gradiente osservato:', eta_calc)
    print('                              (Milioni di anni:', eta_calc/1000000,')')
    print('\n ')
    print('\n ')
 
    return

def npa_age_conductive_cooling_2(Time='Buffon', T_0=1000, k=4):
    
    rho=5510      # Earth mean density (in kg/m3)
    c=1260       # Specific heat for a mixture of mantle components (in J/kg/K)
    
    sec_per_year=31536000
    
    if Time == 'Bibbia':
        max_time_in_Myear=0.001
    if Time == 'Buffon':
        max_time_in_Myear=0.076
    if Time == 'Kelvin':
        max_time_in_Myear=100
    if Time == 'Rutherford':
        max_time_in_Myear=140
    if Time == 'Perry':
        max_time_in_Myear=1300
        
    max_time = sec_per_year*max_time_in_Myear*1000000

              
    grad_calc=sqrt( (rho*c)/(3.14157*k) ) * (T_0/sqrt(max_time))
            
    print('\n ')    
    print('\n Prova inversa - calcolo gradiente da eta\' predefinita:')
    print('\n Valore del gradiente di temperatura in superfice, per l\'Eta\' selezionata:', grad_calc)
    print('\n ')
    print('\n ')
 
    return
